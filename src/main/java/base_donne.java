
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alexis
 */

/* la clase base de donner permet de ce connecter à la base de donner
le string nomé coco permet d'indiquer au programme le chemin de la base de donne
*/
public class base_donne {
    
    protected static String coco;

    public static void setDbPath(String path) {
        base_donne.coco = String.format("jdbc:sqlite:%s", path);
        ///j vile jooser 
    }
    
    // fonction permetant une connection à la base de donner
    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        Connection connection = DriverManager.getConnection(base_donne.coco);
        return connection;
    }
    
    
    public static Statement createStatement(Connection connection) throws SQLException {
        return connection.createStatement();
    }
        
    public static PreparedStatement createPreparedStatement(Connection connection, String query) throws SQLException {
        return connection.prepareStatement(query);
    }

    public static ResultSet executeQuery(Statement statement, String query) throws SQLException {
        return statement.executeQuery(query);
    }
        
    public static ResultSet executePreparedQuery(PreparedStatement preparedStatement) throws SQLException{
        return preparedStatement.executeQuery();
    }
    
}


