
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alexis
 */

/*
  classe comportant comme atribut les éléments qui permette de décrire 
  un objet de la base de donner.
*/
public class class_stock {
    
    protected Integer id;
    protected String titre;
    protected double prix;
    protected Integer joueur;
    protected Integer stock; 
    
    // constructeur
    public class_stock(Integer id, String titre, double prix, Integer joueur, Integer stock) {
        this.titre = titre;
        this.prix = prix;
        this.joueur = joueur;
        this.stock = stock;
        this.id= id;
        
    }
    
    // fonction permetant de ce connecter à la base de donnée 
    // on viens lire tous les éléments conteneu dans la base de donner pour 
    // les stocker dans une liste.
    public static List<class_stock> all() throws SQLException, ClassNotFoundException{
       Connection connection = base_donne.getConnection(); 
       Statement statement = base_donne.createStatement(connection);
       String query = "select * from produit"; 
       ResultSet resultSet = base_donne.executeQuery(statement, query);
       List<class_stock> lignes; 
       lignes = new ArrayList<>(); 
       
       while(resultSet.next()){
            lignes.add(class_stock.createNew(resultSet));
        }
       resultSet.close();
       statement.close();
       connection.close();
       return lignes;
    }
    
    // méthode permetant de supprimer un élément de la base de donnée
    public static void suprimer(int id)  throws SQLException, ClassNotFoundException{
        Connection coco = base_donne.getConnection();
        String query = "delete from produit where id=? ";
        PreparedStatement pst= coco.prepareStatement(query);
        pst.setInt(1,id);
        pst.executeUpdate();
        pst.close();
        coco.close();
    }
     
    // méthode permetant de créer un objet de types class_stock.
    // utilisé dans la méthode all qui stock dans une liste les objets de types class_stock
    public static class_stock createNew(ResultSet resultSet) throws SQLException{
        return new class_stock(
            resultSet.getInt("id"),
            resultSet.getString("titre"), 
            resultSet.getDouble("prix"),
            resultSet.getInt("joueur"), 
            resultSet.getInt("stock")
        );
    }
    
    // méthode permtant d'ajouter un élément en base de donnée
    public static void ajout_element(String titre,double prix, int joueur,int stock) throws SQLException, ClassNotFoundException{
        Connection coco = base_donne.getConnection();
        String query = "insert into produit('titre','prix','joueur','stock') values(?,?,?,?)";
        PreparedStatement pst= coco.prepareStatement(query);
        pst.setString(1,titre);
        pst.setDouble(2,prix);
        pst.setInt(3,joueur);
        pst.setInt(4,stock);
        pst.executeUpdate();
        pst.close();
        coco.close();
    }
    
    
    // méthode permetant de modififier un élément en base de donner.
    public static void modification_element(String titre,double prix, int joueur,int stock,int id) throws SQLException, ClassNotFoundException{
        Connection coco = base_donne.getConnection();
        String query = "update produit set titre=?,prix=?,joueur=?,stock=? where id=?";
        PreparedStatement pst= coco.prepareStatement(query);
        pst.setString(1,titre);
        pst.setDouble(2,prix);
        pst.setInt(3,joueur);
        pst.setInt(4,stock);
        pst.setInt(5,id);
        pst.executeUpdate();
        pst.close();
        coco.close();
    
    }
    
    // retourner un objet.
    public Object[] Objet_contenue_base(){
        return new Object[] { this.id, this.titre, this.prix, this.joueur, this.stock };
    }
    public Integer getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }

    public double getPrix() {
        return prix;
    }

    public Integer getJoueur() {
        return joueur;
    }

    public Integer getStock() {
        return stock;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public void setJoueur(Integer joueur) {
        this.joueur = joueur;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }
    
}
